## Blog built in Symfony 4.2, styled in Bootstrap 3, secured by Auth0

### Requirements
* composer (latest)
* yarn || npm
* php >= 7.0
* mysql-server >= 5.7

### Installation & Preparing
Load dependencies:
```
composer install
yarn install
```
Build js and css:
```
yarn run encore dev --watch
```
Generate the SSH keys:
```
mkdir config/jwt
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```
Insert your data in the `.env` file:
```
JWT_PASSPHRASE=...
AUTH0_CLIENT_ID=...
AUTH0_CLIENT_SECRET=...
AUTH0_DOMAIN=...
DATABASE_URL=mysql://username:password@127.0.0.1:3306/dbname
```
Create DB & fake users:
```
bin/console doctrine:database:create
bin/console doctrine:migration:migrate
bin/console doctrine:fixtures:load
bin/console server:run
```

### Users credentials
| username    | password   | role |
|-------------|------------|------|
|alan@wake.com|123qwe!@#QWE| user |
|lexa@mail.ru|123qwe!@#QWE| admin |

### API commands
Authentication:

| method | url              |
|--------|------------------|
| POST   | /api/login_check |
| POST   | /api/register    |

Resources:

| method     | url                | access          |
|------------|--------------------|-----------------|
| POST       | /api/authors       | admins          |
| GET        | /api/authors/{id?} | all users       |
| PUT/DELETE | /api/authors/{id}  | resource owners |
| POST       | /api/posts         | all users       |
| GET        | /api/posts/{id?}   | all users       |
| PUT/DELETE | /api/posts/{id}    | resource owners |
| GET        | /api/authors/{id}/blog_posts | all users |
 
 **admins have full access**