<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\BlogPost;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class BlogController extends AbstractController
{
    const POST_LIMIT = 3;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \App\Repository\AuthorRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    private $authorRepository;

    /**
     * @var \App\Repository\BlogPostRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    private $blogPostRepository;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->authorRepository = $this->entityManager->getRepository(Author::class);
        $this->blogPostRepository = $this->entityManager->getRepository(BlogPost::class);
    }

    /**
     * @Route("/", name="homepage")
     * @Route("/entries", name="entries")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException If result not unique.
     */
    public function entriesAction(Request $request)
    {
        $page = 1;
        if ($request->get('page')) {
            $page = $request->get('page');
        }

        return $this->render('blog/entries.html.twig', [
            'blogPosts' => $this->blogPostRepository->getAllPosts($page, self::POST_LIMIT),
            'totalBlogPosts' => $this->blogPostRepository->getPostCount(),
            'page' => $page,
            'entryLimit' => self::POST_LIMIT,
        ]);
    }

    /**
     * @Route("/entry/{slug}", name="entry")
     * @param BlogPost $blogPost
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function entryAction(BlogPost $blogPost)
    {
        if (!$blogPost) {
            $this->addFlash('error', 'Unable to find entry!');
            return $this->redirectToRoute('entries');
        }

        return $this->render('blog/entry.html.twig', [
            'blogPost' => $blogPost,
        ]);
    }

    /**
     * @Route("/author/{username}", name="author")
     *
     * @ParamConverter("username", class="App:Author", converter="enescape_username_converter")
     *
     * @param Author $author
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function authorAction(Author $author)
    {
        if (!$author) {
            $this->addFlash('error', 'Unable to find author!');
            return $this->redirectToRoute('entries');
        }

        return $this->render('blog/author.html.twig', [
            'author' => $author,
        ]);
    }
}
