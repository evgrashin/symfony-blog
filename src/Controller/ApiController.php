<?php

namespace App\Controller;

use App\Entity\Author;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Doctrine\UserManager as UserManager;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ApiController extends Controller
{
    private $passwordUpdater;
    private $cononicalFieldUpdater;
    private $objectManager;
    private $class;

    /**
     * ApiController constructor.
     * @param PasswordUpdaterInterface $passwordUpdater
     * @param CanonicalFieldsUpdater   $canonicalFieldsUpdater
     * @param ObjectManager            $om
     * @param string                   $class
     */
    public function __construct(
        PasswordUpdaterInterface $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        ObjectManager $om,
        string $class
    ) {
        $this->passwordUpdater = $passwordUpdater;
        $this->cononicalFieldUpdater = $canonicalFieldsUpdater;
        $this->objectManager = $om;
        $this->class = $class;
    }

    /**
     * @Route("/api/register")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postUserAction(Request $request)
    {
        $content = $request->getContent();

        if (empty($content)) {
            $response = new JsonResponse();
            $response->setData([
                "message" => "User details is empty.",
                "status" => "error",
            ]);
            return $response;
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $user = $serializer->deserialize($content, Author::class, 'json');

        $userManager = new UserManager(
            $this->passwordUpdater,
            $this->cononicalFieldUpdater,
            $this->objectManager,
            $this->class
        );

        $username = $user->getUsername();
        $email = $user->getEmail();

        $usernameExist = $userManager->findUserByUsername($username);
        $emailExist = $userManager->findUserByEmail($email);

        if ($usernameExist || $emailExist) {
            $response = new JsonResponse();
            $response->setData([
                "message" => "User with given Username (".$username.") or Email (".$email.") already exists.",
                "status" => "error",
            ]);
            return $response;
        }

        try {
            /** @var Author $user */
            $userManager->updateUser($user);
        } catch (NotNullConstraintViolationException $exception) {
            $response = new JsonResponse();
            $response->setData([
                "message" => "Not enough data. Username, plainPassword, email, name, title, company or shortBio is empty.",
                "status" => "error",
            ]);
            return $response;
        }

        $response = new JsonResponse();
        $response->setData([
            "message" => "User ".$user->getUsername()." was registered successfully.",
            "status" => "ok",
        ]);
        return $response;
    }
}