<?php

namespace App\Validator\Constains;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ContainsNumeric extends Constraint
{
    /**
     * @var string
     */
    public $message = "Field can contains only numbers.";
}