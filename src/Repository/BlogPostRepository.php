<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method BlogPost      findOneBySlug(string $string)
 * @method Author        findByAuthor(Author $author)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    const DEFAULT_PAGE           = 1;
    const DEFAULT_POST_LIMIT     = 3;
    const DEFAULT_SORT_FIELD     = 'id';
    const DEFAULT_SORT_DIRECTION = 'DESC';

    /**
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlogPost::class);
    }

    /**
     * @param integer $page
     * @param integer $limit
     * @return BlogPost[]
     */
    public function getAllPosts(
        int $page = self::DEFAULT_PAGE,
        int $limit = self::DEFAULT_POST_LIMIT
    ) {
        return $this->createQueryBuilder('bp')
            ->orderBy('bp.' . self::DEFAULT_SORT_FIELD, self::DEFAULT_SORT_DIRECTION)
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return integer
     * @throws NonUniqueResultException If result not unique.
     */
    public function getPostCount()
    {
        return $this->createQueryBuilder('bp')
            ->select('count(bp)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
