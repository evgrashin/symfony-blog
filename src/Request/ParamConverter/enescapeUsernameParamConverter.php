<?php

namespace App\Request\ParamConverter;

use App\Entity\Author;
use Http\Discovery\Exception\NotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

class enescapeUsernameParamConverter implements ParamConverterInterface
{
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    public function supports(ParamConverter $configuration)
    {
        // Если ни один entity manager не обнаружен, значит сконфигурован только Doctrine DBAL
        // В этом случае больше мы ничего сделать не можем и уходим отсюда
        if (null === $this->registry || !count($this->registry->getManagers())) {
            return false;
        }

        // Проверям или в параметрах роута указана опция class
        if (null === $configuration->getClass()) {
            return false;
        }

        return true;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        // Находим актуальный entity manager для класса, с которым будем работать
        $em = $this->registry->getManagerForClass($configuration->getClass());

        if ($em === null) {
            return false;
        }

        // Проверям или указаный класс соответствует тому, что мы хотим
        if ('App\Entity\Author' !== $em->getClassMetadata($configuration->getClass())->getName()) {
            return false;
        }

        $username = $request->attributes->get('username');
        $username = urldecode($username);

        // Проверяем или указанные атрибуты присутствуют в роуте
        if (null === $username) {
            throw new \InvalidArgumentException('Route attribute is missing');
        }

        // Находим актуальный entity manager для класса, с которым будем работать
        $em = $this->registry->getManagerForClass($configuration->getClass());

        /** @var \App\Repository\AuthorRepository $authorRepository */
        $authorRepository = $em->getRepository($configuration->getClass());

        // Пробуем найти автора по слагу
        $author = $authorRepository->findOneByUsername($username);

        // Проверяем или нашли, если нет - кидаем подходящий Exception
        if (null === $author || !($author instanceof Author)) {
            throw new NotFoundException(sprintf('%s object not found.', $configuration->getClass()));
        }

        // Мапим найденого автора к параметрам роута
        $request->attributes->set($configuration->getName(), $author->getUsername());
    }
}